package de.hbrs.wirschiffendas.softwareservice.kafka.consumer.configuration;

import de.hbrs.wirschiffendas.data.entity.Configuration;
import de.hbrs.wirschiffendas.softwareservice.SoftwareServiceApplication;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

@Component
public class KafkaConfigurationConsumer {

    private SoftwareServiceApplication softwareServiceApplication;

    public KafkaConfigurationConsumer(SoftwareServiceApplication softwareServiceApplication) {
        this.softwareServiceApplication = softwareServiceApplication;
    }

    @KafkaListener(topics = "ooka_hauserweber_configuraton_softwareservice", groupId = "ookahauserweber.wirschiffendas.software_consumer")
    public void listenConfiguration (Configuration configuration) {
        System.out.println("Kafka: Message eingegangen");
        softwareServiceApplication.checkConfig(configuration);
    }
}
