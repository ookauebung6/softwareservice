package de.hbrs.wirschiffendas.softwareservice.kafka.producer;

import de.hbrs.wirschiffendas.data.entity.AlgorithmIdentifier;
import de.hbrs.wirschiffendas.data.entity.Status;
import de.hbrs.wirschiffendas.data.entity.TransferItems.StatusTransferItem;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.StringSerializer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;

import java.util.Properties;

@Component
public class KafkaStatusProducer {

    @Autowired
    private KafkaTemplate<String, StatusTransferItem> kafkaTemplate;

    public void sendStatus (Status newStatus) throws Exception {
        StatusTransferItem statusTransferItem = new StatusTransferItem(AlgorithmIdentifier.SOFTWARE, newStatus);
        new KafkaProducerConfig().kafkaTemplate().send("ooka_hauserweber_status", statusTransferItem);
    }

}
